import { stringify } from 'qs';
import request from '../utils/request';

// export async function queryProjectNotice() {
//   return request('/api/project/notice');
// }

// export async function queryActivities() {
//   return request('/api/activities');
// }


// export async function removeRule(params) {
//   return request('/api/rule', {
//     method: 'POST',
//     body: {
//       ...params,
//       method: 'delete',
//     },
//   });
// }

// export async function addRule(params) {
//   return request('/api/rule', {
//     method: 'POST',
//     body: {
//       ...params,
//       method: 'post',
//     },
//   });
// }

// export async function fakeSubmitForm(params) {
//   return request('/api/forms', {
//     method: 'POST',
//     body: params,
//   });
// }

// export async function fakeChartData() {
//   return request('/api/fake_chart_data');
// }

// export async function queryTags() {
//   return request('/api/tags');
// }

// export async function queryBasicProfile() {
//   return request('/api/profile/basic');
// }

// export async function queryAdvancedProfile() {
//   return request('/api/profile/advanced');
// }

// export async function queryFakeList(params) {
//   return request(`/api/fake_list?${stringify(params)}`);
// }

// export async function fakeAccountLogin(params) {
//   return request('/api/login/account', {
//     method: 'POST',
//     body: params,
//   });
// }

// export async function fakeRegister(params) {
//   return request('/api/register', {
//     method: 'POST',
//     body: params,
//   });
// }
// 
// export async function queryNotices() {
//   return request('/api/notices');
// }

var HOST = "http://45.76.195.7:7788"

export async function queryRule(params) {
  return request(HOST+`/api/article/list/page?${stringify(params)}`);
}

export async function searchRule(params) {
  return request(HOST+`/api/article/search/page?${stringify(params)}`);
}

export async function updatePubStatus(params) {
  return request(HOST+`/api/article/update`, {
        method: 'POST',
        body: params,
      });
}

var AHOST = "http://45.76.195.7:7789"
// var AHOST = "http://127.0.0.1:7789"


export async function queryTodays(params) {
  return request(AHOST+`/api/account/today`);
}

export async function queryAccounts(params) {
  return request(AHOST+`/api/account/list`);
}

export async function queryAutoTasks(params) {
  return request(AHOST+`/api/autoTask/list/page?${stringify(params)}`);
}

export async function accounts(params) {
  return request(AHOST+`/api/account/list?`);
}

export async function accountArticle(params) {
  return request(AHOST+`/api/accountArticle/list/page?${stringify(params)}`);
}

export async function queryTask(params) {
  return request(AHOST+`/api/task/list/page?${stringify(params)}`);
}

export async function republish(params) {
  return request(AHOST+`/api/task/republish?${stringify(params)}`);
}

export async function addAutoTask(params) {
  return request(AHOST+`/api/autoTask/add`, {
        method: 'POST',
        body: params,
      });
}


export async function addTask(params) {
  return request(AHOST+`/api/task/add`, {
        method: 'POST',
        body: params,
      });
}

export async function updateCookie(params) {
  return request(AHOST+`/api/account/update`, {
        method: 'POST',
        body: params,
      });
}

