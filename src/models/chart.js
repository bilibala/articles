import { queryTodays } from '../services/api';

export default {
  namespace: 'chart',

  state: {
    todays: {},
    loading: false,
  },

  effects: {
    *fetch(_, { call, put }) {
      const response = yield call(queryTodays);
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
  },

  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        todays:payload,
      };
    },
  },
};
