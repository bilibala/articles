import { queryRule,searchRule,republish,addAutoTask,queryAutoTasks,accountArticle,addTask,accounts, queryTask,removeRule, addRule,queryAccounts,updateCookie,updatePubStatus } from '../services/api';
import { message } from 'antd';

export default {
  namespace: 'rule',

  state: {
    data: {
      list: [],
      page:1,
      count:0,
      page_size:10,
    },
    accounts: {
      list: [],
      page:1,
      count:0,
      page_size:20,
    },
    accountArticle:{
      list: [],
      page:1,
      count:0,
      page_size:10,
    },
    pubAccounts:[],
    searchData: {
      list: [],
      page:1,
      count:0,
      page_size:10,
    },
    tasks:{
      list: [],
      page:1,
      count:0,
      page_size:10,
    },
    autoTasks:{
      list: [],
      page:1,
      count:0,
      page_size:10,
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(queryRule, payload);
      
      yield put({
        type: 'save',
        payload: response.data,
      });
    },
    *search({ payload }, { call, put }) {
      const response = yield call(searchRule, payload);

      yield put({
        type: 'searchsave',
        payload: response.data,
      });
    },
    *fetchAccounts({ payload }, { call, put }) {
      const response = yield call(queryAccounts, payload);
      
      yield put({
        type: 'accounts',
        payload: response.data,
      });
    },
    *fetchAccountArticles({ payload }, { call, put }) {
      const response = yield call(accountArticle, payload);
      
      yield put({
        type: 'accountArticle',
        payload: response.data,
      });
    },
    *fetchPubAccounts({ payload }, { call, put }) {
      const response = yield call(accounts, payload);
      
      yield put({
        type: 'pubaccounts',
        payload: response.data,
      });
    },
    *autofetch({ payload }, { call, put }) {
      const response = yield call(queryAutoTasks, payload);
      
      yield put({
        type: 'autoTasks',
        payload: response.data,
      });
    }, 
    *fetchTasks({ payload }, { call, put }) {
      const response = yield call(queryTask, payload);
      
      yield put({
        type: 'tasks',
        payload: response.data,
      });
    },
    *updateCookie({ payload, callback }, { call, put }) {
      const response = yield call(updateCookie, payload);
      if (callback) callback();
    },
    *updatePubStatus({ payload, callback }, { call, put }) {
      const response = yield call(updatePubStatus, payload);
      if (callback) callback();
    },
    *republish({ payload, callback }, { call, put }) {
      const response = yield call(republish, payload);
      if (callback) callback();
    },
    *addTask({ payload, callback }, { call, put }) {
      console.log("发布:",payload)
      const response = yield call(addTask, payload);
      console.log("发布失败:",response)

      if(response.code == 200){
        message.success('已添加到发布队列')
      }else{
        console.log("发布失败:",response)
        message.error("错误:",response.message)
      }
      if (callback) callback();
    },
    *addAutoTask({ payload, callback }, { call, put }) {
      console.log("发布自动任务:",payload)
      const response = yield call(addAutoTask, payload);
      if(response.code == 200){
        message.success('创建任务成功')
      }else{
        console.log("发布失败:",response.message)
        message.error("错误:",response.message)
      }
      if (callback) callback();
    },
    *remove({ payload, callback }, { call, put }) {
      const response = yield call(removeRule, payload);
      yield put({
        type: 'save',
        payload: response,
      });
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      console.log("save",action.payload)
      return {
        ...state,
        data: {
          list : action.payload.data,
          page:action.payload.page,
          count:action.payload.count,
          page_size:action.payload.page_size,
        },
      };
    },
    searchsave(state, action) {
      console.log("search",action.payload)
      return {
        ...state,
        searchData: {
          list : action.payload.data,
          page:action.payload.page,
          count:action.payload.count,
          page_size:action.payload.page_size,
        },
      };
    },
    accounts(state, action) {
      return {
        ...state,
        accounts: {
          list : action.payload,
        },
      };
    },
    accountArticle(state, action) {
      return {
        ...state,
        accountArticle: {
          list : action.payload.data,
          page:action.payload.page,
          count:action.payload.count,
          page_size:action.payload.page_size,
        },
      };
    },
    pubaccounts(state, action) {
      return {
        ...state,
        pubAccounts: action.payload,
      };
    },
    tasks(state, action) {
      return {
        ...state,
        tasks: {
          list : action.payload.data,
          page:action.payload.page,
          count:action.payload.count,
          page_size:action.payload.page_size,
        },
      };
    },
    autoTasks(state, action) {
      return {
        ...state,
        autoTasks: {
          list : action.payload.data,
          page:action.payload.page,
          count:action.payload.count,
          page_size:action.payload.page_size,
        },
      };
    },
  },
};
