import { isUrl } from '../utils/utils';

const menuData = [
  
  {
    name: '统计面板',
    icon: 'table',
    path: 'dashboard',
    children: [
      {
        name: '统计',
        path: 'analysis',
      }
    ],
  },
  {
    name: '账号管理',
    icon: 'table',
    path: 'account',
    children: [
      {
        name: '账号管理',
        path: 'list',
      },
      {
        name: '文章管理',
        path: 'article',
      }
    ],
  },
  {
    name: '机选列表',
    icon: 'table',
    path: 'article',
    children: [
      {
        name: '天天快报',
        path: 'list',
      },
      {
        name: '文章搜索',
        path: 'search',
      }
    ],
  },
  {
    name: '发布系统',
    icon: 'table',
    path: 'publish',
    children: [
      {
        name: '发布列表',
        path: 'list',
      },
      {
        name: '自动发布',
        path: 'autolist',
      }
    ],
  },
];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map(item => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
