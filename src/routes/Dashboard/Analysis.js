import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import {
  Row,
  Col,
  Icon,
  Card,
  Tabs,
  Table,
  Radio,
  DatePicker,
  Tooltip,
  Menu,
  Dropdown,
} from 'antd';
import numeral from 'numeral';
import {
  ChartCard,
  yuan,
  MiniArea,
  MiniBar,
  MiniProgress,
  Field,
  Bar,
  Pie,
  TimelineChart,
} from 'components/Charts';
import Trend from 'components/Trend';
import NumberInfo from 'components/NumberInfo';
import { getTimeDistance } from '../../utils/utils';

import styles from './Analysis.less';

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

const rankingListData = [];
for (let i = 0; i < 7; i += 1) {
  rankingListData.push({
    title: `工专路 ${i} 号店`,
    total: 323234,
  });
}

@connect(({ chart, loading }) => ({
  chart,
  loading: loading.effects['chart/fetch'],
}))
export default class Analysis extends Component {
  state = {
    // salesType: 'all',
    // currentTabKey: '',
    // rangePickerValue: getTimeDistance('year'),
  };

  componentDidMount() {
    this.props.dispatch({
      type: 'chart/fetch',
    });
  }

  componentWillUnmount() {
    // const { dispatch } = this.props;
    // dispatch({
    //   type: 'chart/clear',
    // });
  }

  render() {
    // const { rangePickerValue, salesType, currentTabKey } = this.state;
    const { chart:{todays}, loading } = this.props;
 
    const topColResponsiveProps = {
      xs: 24,
      sm: 12,
      md: 12,
      lg: 12,
      xl: 6,
      style: { marginBottom: 24 },
    };

    var cards = []
    console.log(todays)
    for(var key in todays){
      if (todays[key].length == 2){
        cards.push((
          <Col key={key} {...topColResponsiveProps}>
            <ChartCard
              bordered={false}
              title={key}
              action={
                <Tooltip title="指标说明">
                  <Icon type="info-circle-o" />
                </Tooltip>
              }
              total={<span> 阅读:{todays[key][1].ReadCount-todays[key][0].ReadCount}</span>}
              footer={(
                <div>
                <span className={styles.trendText}>昨日总阅读:{todays[key][0].ReadCount}</span>
                <span className={styles.trendText}>粉丝:{todays[key][0].FansCount}</span>
              </div>
              )}
              contentHeight={46}
            >
              <Trend style={{ marginRight: 16 }}>
                <span className={styles.trendText}>总阅读:{todays[key].length > 0? todays[key][1].ReadCount: "未知"}</span>
              </Trend>
              <Trend >
                <span className={styles.trendText}>粉丝:{todays[key].length > 0? todays[key][1].FansCount: "未知"}</span>
              </Trend>
            </ChartCard>
          </Col>
      ))
      }
    }

    return (
      <Fragment>
        <Row gutter={24}>
          {cards}
        </Row>

      </Fragment>
    );
  }
}
